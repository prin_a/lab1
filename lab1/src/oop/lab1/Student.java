package oop.lab1;

//TODO Write class Javadoc
/**
 * A simple model for a student with a name.
 * 
 * @author Prin Angkunanuwat
 */
public class Student extends Person {
	private long id;
	
	//TODO Write constructor Javadoc
	/**
	 * Initialize a new student object.
	 * @param name is the name of the new Student
	 * @param id is the id of the new Student
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	/**
	 * Compare student's by name.
	 * They are equal if the id matches.
	 * @param obj is another Object to compare to this one.
	 * @return true if the id is same, false otherwise.
	 */
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (obj.getClass() != this.getClass()) return false;
		Student other = (Student) obj;
		if (this.id == other.id) 
			return true ;
		return false;
	}
}

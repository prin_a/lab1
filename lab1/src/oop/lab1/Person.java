package oop.lab1;
/**
 * A simple model for a person with a name.
 * 
 * @author Prin Angkunanuwat
 */
public class Person {
	/** the person's name. may contain spaces. */
	private String name;
	
	/**
	 * Initialize a new Person object.
	 * @param name is the name of the new Person
	 */
	public Person(String name) {
		this.name = name;
	}
	
	/**
	 * Get the person's name.
	 * @return the name of this person
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Compare person's by name.
	 * They are equal if the name matches.
	 * @param obj is another Object to compare to this one.
	 * @return true if the name is same, false otherwise.
	 */
	public boolean equals(Object obj) {
		
		//TODO After running the test program (Main), fix this method.
		if (obj == null) return false;
		if (obj.getClass() != this.getClass()) return false;
		Person other =  (Person) obj;
		if ( name.equalsIgnoreCase( other.name ) )
			return true;
		return false; 
	}
	
	/**
	 * Get a string representation of this Person.
	 */
	public String toString() {
		return "Person " + name;
	}
}
